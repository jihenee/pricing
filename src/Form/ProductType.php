<?php
/**
 * Created by PhpStorm.
 * User: tritux
 * Date: 02/03/20
 * Time: 10:37 ص
 */
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use App\Entity\Produit;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prix',  MoneyType::class, [
            ])
            ->add('vendeur')
            ->add('etat', ChoiceType::class, [
                'choices'  => [
                    'Etat moyen' => 1,
                    'Bon etat' => 2,
                    'Tres bon état' => 3,
                    'Comme neuf' => 4,
                    'Neuf' => 5,
                ]
                    ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
            'csrf_protection' => false,
        ]);
    }
}