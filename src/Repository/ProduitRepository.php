<?php

namespace App\Repository;

use App\Entity\Produit;

use Doctrine\ORM\EntityRepository;

/**
 * Class ProduitRepository
 * @package App\Repository
 */
class ProduitRepository extends EntityRepository
{
    public function findBestEtat($etat)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p.prix as prix, p.etat')
            ->where('p.etat >= :etat')
            ->setParameter('etat', $etat)
             ->orderBy('p.prix','asc')
        ;
        return $qb->getQuery()->getResult();

    }

    public function findAllBestEtat()
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->orderBy('p.etat','desc');
        return $qb->getQuery()->getResult();
    }
}
