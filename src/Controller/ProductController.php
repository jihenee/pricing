<?php
/**
 * Created by PhpStorm.
 * User: tritux
 * Date: 02/03/20
 * Time: 11:06 ص
 */

namespace App\Controller;

use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Produit;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ProductController
 */
class ProductController extends AbstractController
{
    protected static $model = Produit::class;

    /**
     * Creer une nouvelle entite produit.
     *
     * @Route("/new", name="product_new")
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //Creer un nouveau produit
        $produit = new Produit();
        $form = $this->createForm(ProductType::class, $produit);
        $form->handleRequest($request);
        $price = $produit->getPrix();
        $etat = $produit->getEtat();
        //Appel à la fonction qui vérifie les etats
        $result = $this->checkOtherPriceAction($price, $etat);
        if ($form->isSubmitted() && $form->isValid()) {
            $produit->setPrix($result);
            $em->persist($produit);
            $em->flush();
            return $this->redirectToRoute('product_show');
        }
        return $this->render('product/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/", name="product_show")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        // Afficher les vendeurs triés par état descencdant
        $products = $em->getRepository(Produit::class)->findAllBestEtat();
        return $this->render('product/index.html.twig',
            array(
                'products' => $products
            )
            );

    }

    public function checkOtherPriceAction($price, $etat)
    {
        $em = $this->getDoctrine()->getManager();
        $find = $em->getRepository(Produit::class)->findBestetat($etat);
        // on teste s'il ne s'agit pas du premier produit à ajouter
        if (count($find) != 0) {
            $i = 0;
            // on boucle jusqu'à trouver un prix supérieur au prix plancher proposé
            while (($find[$i]['prix'] < $price) and ($i > count($find))) {
                $i++;
            }
            // Si le meme etat trouvé on diminue de 1 centime
            if ($find[$i]['etat'] == $etat and $find[$i]['prix'] > $price)
                $price = $find[$i]['prix'] - 0.01;
            // Si l'etat du concurrent est meilleur que le sien
            elseif ($find[$i]['etat'] > $etat)
                $price = $find[$i]['prix'] - 1;
        }
        return $price;
    }
}
